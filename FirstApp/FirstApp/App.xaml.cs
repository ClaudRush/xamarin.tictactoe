﻿using Autofac;
using FirstApp.IoC;
using FirstApp.Views;
using Xamarin.Forms;

namespace FirstApp
{
    public partial class App : Application
    {
        public App(Module platformModule)
        {
            var container = InitializeDependencies(platformModule);
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage());
        }

        protected IContainer InitializeDependencies(Module platformModule)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new BaseModule());
            builder.RegisterModule(platformModule);

            return builder.Build();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
