﻿namespace FirstApp.Models
{
    public interface IClickModel
    {
        int ClicksCount { get; }

        void OnClick();
        void SetClickCount(int count);
        void Reset();
    }
}
