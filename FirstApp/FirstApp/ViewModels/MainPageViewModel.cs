﻿using FirstApp.Views;
using Prism.Commands;
using System.Windows.Input;
using Xamarin.Forms;

namespace FirstApp.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        public ICommand PlayCommand { get; private set; }

        public MainPageViewModel()
        {
            PlayCommand = new DelegateCommand(() => NavigateToGamePage());
        }

        private async void NavigateToGamePage()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new GamePage());
        }
    }
}
