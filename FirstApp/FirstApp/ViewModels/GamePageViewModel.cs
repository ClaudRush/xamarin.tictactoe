﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace FirstApp.ViewModels
{
    public class GamePageViewModel : BaseViewModel
    {
        public ICommand SetSymbolCommand { get; private set; }

        private string _imagePath1;
        public string ImagePath1 { 
            get => _imagePath1;
            private set
            {
                _imagePath1 = value;
                NotyfyPropertyChanged(nameof(ImagePath1));
            }
        }

        public GamePageViewModel()
        {
            SetSymbolCommand = new Command(SetSymbol);
        }

        private void SetSymbol()
        {
           
        }
    }
}
