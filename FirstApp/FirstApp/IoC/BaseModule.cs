﻿using Autofac;
using FirstApp.ViewModels;

namespace FirstApp.IoC
{
    public class BaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<MainPageViewModel>();
        }
    }
}
