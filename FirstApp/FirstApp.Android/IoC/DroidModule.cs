﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Autofac;
using FirstApp.Droid.Models;
using FirstApp.Models;

namespace FirstApp.Droid.IoC
{
    public class DroidModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<ClickModel>().As<IClickModel>();
        }
    }
}