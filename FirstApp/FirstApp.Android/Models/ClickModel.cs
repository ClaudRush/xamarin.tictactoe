﻿using FirstApp.Models;

namespace FirstApp.Droid.Models
{
    public class ClickModel : IClickModel
    {
        public int ClicksCount { get; private set; }

        public void OnClick()
        {
            ClicksCount++;
        }

        public void SetClickCount(int count)
        {
            ClicksCount = count;
        }

        public void Reset()
        {
            ClicksCount = 0;
        }
    }
}